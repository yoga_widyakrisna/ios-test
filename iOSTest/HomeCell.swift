//
//  HomeCell.swift
//  iOSTest
//
//  Created by Yoga Widyakrisna on 10/26/16.
//  Copyright © 2016 Yoga Widyakrisna. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var labelPlace: UILabel!
    @IBOutlet weak var labelDegree: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
