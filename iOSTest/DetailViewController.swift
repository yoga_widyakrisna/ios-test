//
//  DetailViewController.swift
//  iOSTest
//
//  Created by Yoga Widyakrisna on 10/27/16.
//  Copyright © 2016 Yoga Widyakrisna. All rights reserved.
//

import UIKit
import RealmSwift

class DetailViewController: UIViewController {

    // Property
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var labelWeather: UILabel!
    @IBOutlet weak var labelDegree: UILabel!
    @IBOutlet weak var labelHumidity: UILabel!
    @IBOutlet weak var labelWind: UILabel!
    @IBOutlet weak var labelCloudiness: UILabel!
    @IBOutlet weak var labelSunrise: UILabel!
    @IBOutlet weak var labelSunset: UILabel!
    let realm = try! Realm()
    var imageWeatherString = String()
    var cityString = String()
    var weatherString = String()
    var degreeString = String()
    var humidityString = String()
    var windString = String()
    var cloudinessString = String()
    var sunriseTime = NSTimeInterval()
    var sunsetTime = NSTimeInterval()
    var selectedIndex = NSIndexPath()
    var isCurrent = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if(isCurrent){
            self.btnDelete.hidden = true
        }
        
        // Change Image
        if let imageURL = NSURL(string: "http://openweathermap.org/img/w/\(self.imageWeatherString).png") {
            self.imageWeather.setImageWithURL(imageURL)
        }        
        self.labelCity.text = self.cityString
        self.labelWeather.text = self.weatherString
        self.labelDegree.text = self.degreeString
        self.labelHumidity.text = self.humidityString
        self.labelWind.text = self.windString
        self.labelCloudiness.text = self.cloudinessString
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        let dateSunrise = NSDate(timeIntervalSince1970: sunriseTime)
        let sunriseString = dateFormatter.stringFromDate(dateSunrise)
        let dateSunset = NSDate(timeIntervalSince1970: sunsetTime)
        let sunsetString = dateFormatter.stringFromDate(dateSunset)
        
        self.labelSunrise.text = sunriseString
        self.labelSunset.text = sunsetString

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deleteCity(sender: AnyObject) {
        let allObjects = self.realm.objects(City)
        var byName = allObjects.sorted("name", ascending: true)
        // Delete an object with a transaction
        try! realm.write {
            realm.delete(byName[selectedIndex.row])
        }
        navigationController?.popToRootViewControllerAnimated(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
