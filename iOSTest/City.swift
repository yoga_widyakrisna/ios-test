//
//  City.swift
//  iOSTest
//
//  Created by Yoga Widyakrisna on 10/28/16.
//  Copyright © 2016 Yoga Widyakrisna. All rights reserved.
//

import Foundation
import RealmSwift

class City: Object {
    
    dynamic var name = String()
    dynamic var weatherImageString = String()
    dynamic var degree = Int()
    dynamic var humidity = Int()
    dynamic var weather = String()
    dynamic var wind = Double()
    dynamic var cloudiness = Int()
    dynamic var sunriseTime = NSTimeInterval()
    dynamic var sunsetTime = NSTimeInterval()
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
