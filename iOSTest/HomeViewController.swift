//
//  HomeViewController.swift
//  iOSTest
//
//  Created by Yoga Widyakrisna on 10/25/16.
//  Copyright © 2016 Yoga Widyakrisna. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import GooglePlaces
import RealmSwift
import MapleBacon

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {

    // Property
    @IBOutlet weak var refreshCurrent: UIButton!
    var refreshControl: UIRefreshControl!
    @IBOutlet weak var contentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var labelWeather: UILabel!
    @IBOutlet weak var labelDegree: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var detailViewController: DetailViewController?
    let locationManager = CLLocationManager()
    let realm = try! Realm()
    var url = ""
    var weather = String()
    var imageWeatherString = String()
    var degree = Int()
    var humidity = Int()
    var wind = Double()
    var cloudiness = Int()
    var sunriseTime = NSTimeInterval()
    var sunsetTime = NSTimeInterval()
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Refresher
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(HomeViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(refreshControl) // not required when using UITableViewController
        
        // Update Current Location
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()

    }
    
    func refresh(sender:AnyObject)
    {
        // Updating your data here...
        
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
        let userLocation:CLLocation = locations[0]
        
        CLGeocoder().reverseGeocodeLocation(userLocation, completionHandler: {(placemarks, error) -> Void in
            
            if error != nil {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0] 
                let city:String = (pm.locality != nil) ? pm.locality! : ""
                self.url = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&APPID=e49cf10c9a7e49cbe14c46c4475744dd&units=metric"
                self.url = self.url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
                
                Alamofire.request(.POST, self.url, encoding:.JSON).responseJSON
                    { response in switch response.result {
                    case .Success(let JSON):
                        let response = JSON as! NSDictionary
                        
                        // Retrieve
                        let weatherArray = response.objectForKey("weather") as? NSArray
                        let weatherDict = weatherArray![0] as? NSDictionary
                        let mainDict = response.objectForKey("main") as? NSDictionary
                        let windDict = response.objectForKey("wind") as? NSDictionary
                        let cloudDict = response.objectForKey("clouds") as? NSDictionary
                        let sysDict = response.objectForKey("sys") as? NSDictionary
                        self.imageWeatherString = (weatherDict!.objectForKey("icon") as? String)!
                        self.weather = (weatherDict!.objectForKey("main") as? String)!
                        self.degree = (mainDict!.objectForKey("temp") as? Int)!
                        self.humidity = (mainDict!.objectForKey("humidity") as? Int)!
                        self.wind = (windDict!.objectForKey("speed") as? Double)!
                        self.cloudiness = (cloudDict!.objectForKey("all") as? Int)!
                        self.sunriseTime = (sysDict!.objectForKey("sunrise") as? NSTimeInterval)!
                        self.sunsetTime = (sysDict!.objectForKey("sunset") as? NSTimeInterval)!
                        
                        // Change Text
                        self.labelCity.text = city
                        self.labelWeather.text = self.weather
                        self.labelDegree.text = "\(self.degree)°"
                        
                        // Change Image
                        if let imageURL = NSURL(string: "http://openweathermap.org/img/w/\(self.imageWeatherString).png") {
                            self.imageWeather.setImageWithURL(imageURL)
                        }
                        
                    case .Failure(let error):
                        print("Request failed with error: \(error)")
                        }
                }
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    @IBAction func findMyLocation(sender: AnyObject) {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func addCity(sender: AnyObject) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.presentViewController(autocompleteController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let allObjects = self.realm.objects(City)
        return allObjects.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let allObjects = self.realm.objects(City)
        var byName = allObjects.sorted("name", ascending: true)
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("homeCell", forIndexPath: indexPath) as! HomeCell
        
        cell.labelPlace.text = byName[indexPath.row].name
        var degree = byName[indexPath.row].degree
        cell.labelDegree.text = "\(degree)°"
        
        return cell
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detailCurrentSegue" {
            let detailViewController: DetailViewController = segue.destinationViewController as! DetailViewController
            detailViewController.cityString = self.labelCity.text!
            detailViewController.weatherString = self.labelWeather.text!
            detailViewController.degreeString = self.labelDegree.text!
            detailViewController.humidityString = "\(self.humidity)"
            detailViewController.windString = "\(self.wind)"
            detailViewController.cloudinessString = "\(self.cloudiness)"
            detailViewController.sunriseTime = self.sunriseTime
            detailViewController.sunsetTime = self.sunsetTime
            detailViewController.isCurrent = true
            detailViewController.imageWeatherString = self.imageWeatherString
            
        }
        if segue.identifier == "detailSavedListSegue" {
            let allObjects = self.realm.objects(City)
            var byName = allObjects.sorted("name", ascending: true)
            let selectedIndex = self.tableView.indexPathForCell(sender as! HomeCell)
            let detailViewController: DetailViewController = segue.destinationViewController as! DetailViewController
            detailViewController.cityString = byName[(selectedIndex?.row)!].name
            detailViewController.imageWeatherString = byName[(selectedIndex?.row)!].weatherImageString
            detailViewController.weatherString = byName[(selectedIndex?.row)!].weather
            detailViewController.degreeString = "\(byName[(selectedIndex?.row)!].degree)°"
            detailViewController.humidityString = "\(byName[(selectedIndex?.row)!].humidity)"
            detailViewController.windString = "\(byName[(selectedIndex?.row)!].wind)"
            detailViewController.cloudinessString = "\(byName[(selectedIndex?.row)!].cloudiness)"
            detailViewController.sunriseTime = byName[(selectedIndex?.row)!].sunriseTime
            detailViewController.sunsetTime = byName[(selectedIndex?.row)!].sunsetTime
            detailViewController.selectedIndex = selectedIndex!
            detailViewController.isCurrent = false
            
        }
    }

}

extension HomeViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(viewController: GMSAutocompleteViewController, didAutocompleteWithPlace place: GMSPlace) {
        
        // TO DO : Make Realm Object,Tembak WS, Save ke Realm SMUANYA
        var urlAdd = "http://api.openweathermap.org/data/2.5/weather?q=\(place.name)&APPID=e49cf10c9a7e49cbe14c46c4475744dd&units=metric"
        
        urlAdd = urlAdd.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        Alamofire.request(.POST, urlAdd, encoding:.JSON).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                let response = JSON as! NSDictionary
                
                // Retrieve
                let weatherArray = response.objectForKey("weather") as? NSArray
                let weatherDict = weatherArray![0] as? NSDictionary
                let mainDict = response.objectForKey("main") as? NSDictionary
                let windDict = response.objectForKey("wind") as? NSDictionary
                let cloudDict = response.objectForKey("clouds") as? NSDictionary
                let sysDict = response.objectForKey("sys") as? NSDictionary
                self.imageWeatherString = (weatherDict!.objectForKey("icon") as? String)!
                let cityName = place.name
                let weather = weatherDict!.objectForKey("main") as? String
                self.degree = (mainDict!.objectForKey("temp") as? Int)!
                self.humidity = (mainDict!.objectForKey("humidity") as? Int)!
                self.wind = (windDict!.objectForKey("speed") as? Double)!
                self.cloudiness = (cloudDict!.objectForKey("all") as? Int)!
                self.sunriseTime = (sysDict!.objectForKey("sunrise") as? NSTimeInterval)!
                self.sunsetTime = (sysDict!.objectForKey("sunset") as? NSTimeInterval)!
                
                // Save to Realm DB
                let city = City()
                city.name = cityName
                city.weatherImageString = self.imageWeatherString
                city.weather = weather!
                city.degree = self.degree
                city.humidity = self.humidity
                city.wind = self.wind
                city.cloudiness = self.cloudiness
                city.sunriseTime = self.sunriseTime
                city.sunsetTime = self.sunsetTime
                
                try! self.realm.write({
                    self.realm.add(city)
                    print("Added \(city.name) to Realm!")
                })
                
                let allObjects = self.realm.objects(City)
                for object in allObjects{
                    print("\(object.name) with \(object.degree) C!")
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                }
        }
        self.tableView.reloadData()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        // TODO: handle the error.
        print("Error: ", error.description)
    }
    
    // User canceled the operation.
    func wasCancelled(viewController: GMSAutocompleteViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
}
